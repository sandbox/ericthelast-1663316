Description
-----------
Email Restrict is intended to be a very heavy-handed approach to limiting 
system emails to select users only. Very useful for development sites that
have real users loaded but don't want to accidentally send emails to all 
users (ie using rules to send an email when a user joins an organic group).

While other modules such as the awesome Reroute Email are great for sending 
all mail to one or more email addresses, this module offers the ability to 
select a subset of users that will still still receive email as normal while 
messages destine for those who aren't selected don't get sent.

I created this module after the third time I accidentally blasted the entire 
company with emails after posting content in an organic group (we had to 
disable Reroute Email so that multiple users could get in and test the 
functionality without wiping out all existing accounts.

Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the entire directory and all its contents to your
   modules directory.

Configuration
-------------
To enable this module do the following:

1. Go to Admin -> Modules, and enable Email Restrict.

2. Go to Admin -> Configuration -> Development -> Email Restrict, and select 
   the users that should be allowed to be sent emails to.

Tips and Tricks
---------------
I put in a "nag" notice whenever emails are being restricted (which 
shows up on any form drupal loads) so that you don't forget that you're 
restricting emails.  Also, if no users are selected, then this module
will silently back out and leave emails untouched.
